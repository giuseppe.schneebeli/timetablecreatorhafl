# Copyright 2019, Giuseppe Schneebel

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import lxml.etree
import lxml.html
import requests
import csv
import openpyxl

def n2str(nr): # turns a number to a string source: https://www.hafl.bfh.ch/fileadmin/stundenplaene/hs/untisscripts.js
    string = str(nr)
    while len(string)<5:
        string = "0" + string
    return(string)

def getTimes(): # A Funtion to Extract HAFL Times
    url = 'https://www.hafl.bfh.ch/fileadmin/stundenplaene/hs/P1/f/f00001.htm'
    r = requests.get(url)
    root = lxml.html.fromstring(r.content)
    times = root.find('.//table')
    times = times.getchildren()
    for i in range(len(times)):
        times[i]=times[i].find('.//td')
    times = [x for x in times if x is not None]
    for i in range(len(times)):
        times[i]=times[i].findall('.//font')
        for j in range(len(times[i])):
            times[i][j]=times[i][j].text
            times[i][j]=times[i][j].strip() # strip text from line breaks
    del times[0]
    return(times)

def createbasicxlsx(): # create the Basic empty Timetable
    timetable = openpyxl.Workbook()
    Stundenplan = timetable.get_sheet_by_name('Sheet')
    Stundenplan.cell(row=1, column=1).value = 'Stundenplan'
    row1 = ['Zeit','Montag','Dienstag','Mittwoch','Donnerstag','Freitag']
    for c, i in enumerate(row1):
        Stundenplan.cell(row=2, column=(c+1)).value = i
    times=getTimes()
    for c, i in enumerate(times):
        Stundenplan.cell(row=(3+c),column=1).value = i[0]+'-'+i[1]
    timetable.save('Timetable.xlsx')

def Modulestoxls(modules, week): # A function to write Modules to the Timetable week defines the quarter or Blockwoche
    timetable = openpyxl.load_workbook('Timetable.xlsx')
    Stundenplan = timetable.get_sheet_by_name('Sheet')
    with open((week+'.csv'), newline='') as csvfile:
        datafile = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for module in modules: 
            for row in datafile: # iterate through the Quarter to check if the module take place
                if module == row[0]: # if the module takes place check for the times
                    days = row[1:] 
                    for c,day in enumerate(days): # iterate throu the days
                        i = 0
                        for t in Stundenplan.iter_cols(min_row=3,min_col=1,max_row=17,max_col=1): # iterate throu the Basic Table to find times
                            entry=False
                            for cell in t:
                                i += 1
                                if cell.value[:4]==day[:4]: # if the Time is found
                                    time=i
                                    start_r=2+time
                                    start_c=2+int(c/2)
                                    Stundenplan.cell(row=start_r,
                                            column=start_c).value = module # write the Module to the cell
                                    entry=True
                                if (entry and (cell.value[-4:]==day[-4:])):
                                    time=i # merge the cell over all the lessions
                                    Stundenplan.merge_cells(start_row=start_r, start_column=start_c, end_row=(2+time), end_column=start_c)
            csvfile.seek(0) # set the file back to the beginning
    timetable.save(week+'.xlsx')

def getModules(url0,sem,week): #a funtion to collect Time and Day of a Module out of a html table
    modules = [['Module', 'Monday AM', 'Monday PM', 'Tuesday AM', 'Tuesday PM', 'Wednesday AM', 'Wednesday PM', 'Thursday AM', 'Thursday PM', 'Friday AM', 'Friday PM']]
    i = 1
    url = url0+sem+'/'+week+'/f/f00001.htm'
    r = requests.get(url)
    while r.status_code == requests.codes.ok: # continue as long as we can find online time tables
        root = lxml.html.fromstring(r.content)
        isnotempty = len(root.xpath('//font[@face="Arial"]')) > 40 # if table has less than 40 entries it is empty
        if isnotempty:
            module = root.xpath('//font[@size="6"]/text()') # get the module Name
            module = [module[0].strip()] # strip the string of line breaks
            for y in range(10): # fill the Mudul occurences as empty
                module.append('')
            day=root.xpath('//td[@rowspan>"2"]') # get all fields where the module occure
            for y in range(len(day)): # for every ocurence get day and time
              end = day[y].attrib['rowspan'] #get the number of rowspan to figure out ending time
              parent = day[y].getparent()
              day[y] = parent.index(day[y]) #get the index of the day cell
              day[y] += day[y]-1 # calculate day position of the DB
              time = parent.find('.//font') # find the starting time
              time = time.text
              time = time.strip()
              for row in range(int(end)-2): # find the node with the ending time
                  parent=parent.getnext()
              end = parent.xpath('.//td')
              end = end[0].findall('.//font')
              end = end[1].text
              end = end.strip()
              time = time +'-'+end # write start and end time
              if (time[0]=='1') and (int(time[1])>1): # check afternoon
                  day[y] += 1
              module[day[y]]=time
            print(module) #print the module and its times
            modules.append(module)
        i += 1
        string=n2str(i) # get to the next online table
        url = url0+sem+'/'+week+'/f/f'+string+'.htm'
        r = requests.get(url)
    return(modules)

def getClasses(url0,sem,week): # A function to get the classes and their modules
    url = url0+sem+'/'+week+'/c/c00001.htm'
    classes= []
    i = 1
    r = requests.get(url)
    while r.status_code == requests.codes.ok: # continue as long as there are sites
        root = lxml.html.fromstring(r.content)
        isnotempty = len(root.xpath('//font[@face="Arial"]')) > 40
        if isnotempty:
            schoolclass = root.xpath('//font[@size="6"]/text()') # get the Name of the schoolcass
            schoolclass = [schoolclass[0].strip()] # strip the string of line breaks
            classalready = [False,0]
            for c,y in enumerate(classes): # check if the class is already fetched 
                if schoolclass[0] == y[0]:
                    classalready[0] = True
                    classalready[1] = c
            modules = root.findall('.//b') # check for all the modules in this class
            del modules[:5] # del weekdays
            for y in modules:
                y=y.text
                y=y.strip()
                if y[-1]=='.': # remove ending point
                    y=y[:-1]
                modulalready = False
                if classalready[0]: 
                    for j in classes[classalready[1]]:
                        if j == y: # check if module is already fetched
                            modulalready = True
                    if not modulalready:
                        classes[classalready[1]].append(y)
                else:
                    for j in schoolclass:
                        if j == y:
                            modulalready = True
                    if not modulalready:
                        schoolclass.append(y)
            if not classalready[0]:
                classes.append(schoolclass)
        i += 1
        string=n2str(i) # get url for new onlinetable
        url = url0+sem+'/'+week+'/c/c'+string+'.htm'
        r = requests.get(url)
    return(classes)

def writetocsv(name, array): #a function to write data to csv
    with open((name+'.csv'), 'w', newline='') as csvfile:
        datafile = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in array:
            print('print: ', row)
            datafile.writerow(row)

def getWeeks(url): # get the Quarters
    r = requests.get(url)
    root = lxml.html.fromstring(r.content)
    weeks = [root.xpath('//select[@name="week"]/option/text()')]
    weeks.append(root.xpath('//select[@name="week"]/option/@value'))
    return(weeks)

def collectdata():#a function to collect the weeks
    sem = ['hs','fs']
    url0 = 'https://www.hafl.bfh.ch/fileadmin/stundenplaene/'
    for x in sem: # iterate through the two semester
        url1 = url0+x+'/frames/navbar.htm'
        weeks=getWeeks(url1)
        for c, week in enumerate(weeks[1]):
            print(week)
            modules=getModules(url0,x,week)
            classes=getClasses(url0,x,week)
            writetocsv(weeks[0][c], modules)
    writetocsv('Klassen',classes)    

def getSchoolclasses(): # get the Classes from the csv file
    schoolclasses = []
    with open('Klassen.csv', newline='') as csvfile:
        datafile = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in datafile:
            schoolclasses.append(row[0])
    return(schoolclasses)

def getModulesofschoolclass(schoolclass): # get the Modules from the input class
    with open('Klassen.csv', newline='') as csvfile:
        datafile = csv.reader(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in datafile:
            if schoolclass == row[0]:
                modules = row[1:]
    return(modules)

def formatStringtoPrint(lst): # format a string to multicolon print
    formatstr = []
    if (len(lst)%3)!=0:
        for i in range((3-len(lst)%3)):
            lst.append('')
    for c, val in enumerate(lst):
        formatstr.append('['+str(c)+'] ')
        formatstr.append(val)
        if ((c+1)%3)==0:
            print('{:>5s}{:7s}{:>15s}{:18s}{:>7s}{:25s}'.format(*formatstr))
            del formatstr[0:]

