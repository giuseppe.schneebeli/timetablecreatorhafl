# Copyright 2019, Giuseppe Schneebel

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import TimeTableFunctions as ttf

updatedb = input('Do you want to update the DB? [y/n] ')
if updatedb == 'y':
    ttf.collectdata()
    print('This may take a while, be patient')
createbasic = input('Do you want to update the Basic timetable? [y/n] ')
if createbasic == 'y':
    ttf.createbasicxlsx()
schoolclasses=ttf.getSchoolclasses()
schoolclasses.append('individual')
sem1 = ttf.getWeeks('https://www.hafl.bfh.ch/fileadmin/stundenplaene/hs/frames/navbar.htm')
sem2 = ttf.getWeeks('https://www.hafl.bfh.ch/fileadmin/stundenplaene/fs/frames/navbar.htm')
weeks=sem1[0]+sem2[0]
while True:
    print('For which Quarter do you want to do a TimeTable?')
    ttf.formatStringtoPrint(weeks)
    week=input('type the number: ')
    week=weeks[int(week)]
    print('Choose your class or Individual if you want to create your Timetable from scratch')
    ttf.formatStringtoPrint(schoolclasses)
    index=input('type the number: ')
    choice=schoolclasses[int(index)]
    if choice == 'individual':
        print('Individual Timetable is not yet implemented')
    else:
        modules=ttf.getModulesofschoolclass(choice)
        ttf.formatStringtoPrint(modules)
        print('Your Timetable is ready')
        break
ttf.Modulestoxls(modules, week)
